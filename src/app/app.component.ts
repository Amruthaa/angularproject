import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  // templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  template: `
  <div>
  <button type="button" (click)="add()">+</button>
  <input type="text" name="value" [(ngModel)]="value">
  <button type="button" (click)="minus()">-</button>
  <div *ngFor="let values of value;">
  {{ values.value }}
</div>
</div>
  `
})
export class AppComponent {
  value = 0;
  add() {
    this.value = this.value + 1;
  }

  minus() {
    this.value = this.value - 1;
  }
}
